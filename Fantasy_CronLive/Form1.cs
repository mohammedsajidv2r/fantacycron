﻿using Match_Update_Cron.ClassData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Configuration;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Security.Policy;

namespace Match_Update_Cron
{
    public partial class MatchUpdate : Form
    {

        public string ExeCaption_ = "";
        public MatchUpdate(string ExeCaption)
        {
            ExeCaption_ = ExeCaption;
            InitializeComponent();
        }

        private void MatchUpdateData_Load(object sender, EventArgs e)
        {
            BaseConnection connection = new BaseConnection();
            try
            {
                if (ExeCaption_.ToLower() == "jobupdatematchs")
                {
                    WebClient wclient = new WebClient();
                    string apiurl = ConfigurationManager.AppSettings["MatchApi"];
                    //string apiurl = "https://thehedgingmachine.com/hedgeapi/cricketMarkets";

                    string apidata = wclient.DownloadString(apiurl);
                    JArray jsonArray = JArray.Parse(apidata);
                    List<matchdata> matachObj = new List<matchdata>();
                    for (int i = 0; i < jsonArray.Count; i++)
                    {
                        dynamic data = JObject.Parse(jsonArray[i].ToString());
                        matchdata md = new matchdata();
                        md.name = Convert.ToString(data.SelectToken("competition").SelectToken("name").Value);
                        if (md.name == "Indian Premier League")
                        {
                            md.marketId = Convert.ToString(data.SelectToken("marketId"));
                            md.venue = Convert.ToString(data.SelectToken("event").SelectToken("venue"));
                            DateTime opendate = Convert.ToDateTime(data.SelectToken("marketStartTime"));
                            md.match_date = opendate;
                            md.match_time = opendate.AddHours(5);
                            md.selection_idHome = "1";
                            md.runnerNameHome = Convert.ToString(data.SelectToken("selection")[0].SelectToken("runnerName"));
                            md.lastPriceTradedHome = Convert.ToString(data.SelectToken("selection")[0].SelectToken("odds"));
                            md.selection_idAway = "2";
                            md.runnerNameAway = Convert.ToString(data.SelectToken("selection")[1].SelectToken("runnerName"));
                            md.lastPriceTradedAway = Convert.ToString(data.SelectToken("selection")[1].SelectToken("odds"));
                            md.home_team_id = Convert.ToString(data.SelectToken("selection")[0].SelectToken("teamId"));
                            md.away_team_id = Convert.ToString(data.SelectToken("selection")[1].SelectToken("teamId"));
                            matachObj.Add(md);
                        }
                    }
                    var XMLData = XmlConversion.SerializeToXElement(matachObj);
                    connection.MatchUpdate(ExeCaption_.ToLower(), XMLData.ToString());
                }
                else if (ExeCaption_.ToLower() == "jobupdateodds")
                {
                    List<MatchModel> matchIdList = connection.getMatchId("getMarketMatchlive");
                    WebClient wclient = new WebClient();
                    string apiurl = ConfigurationManager.AppSettings["MatchOddApi"];
                    List<matchdata> matachObj = new List<matchdata>();
                    for (int i = 0; i < matchIdList.Count; i++)
                    {
                        string apiurlId = apiurl + matchIdList[i].match_market_id;
                        string apidata = wclient.DownloadString(apiurlId);
                        JArray jsonArray = JArray.Parse(apidata);
                        dynamic data = JObject.Parse(jsonArray[0].ToString());
                        matchdata md = new matchdata();
                        md.marketId = matchIdList[i].match_market_id;
                        md.match_id = matchIdList[i].match_id;
                        md.selection_idHome = "1";
                        md.lastPriceTradedHome = Convert.ToString(data.SelectToken("selections")[0].SelectToken("odds"));
                        md.selection_idAway = "2";
                        md.lastPriceTradedAway = Convert.ToString(data.SelectToken("selections")[1].SelectToken("odds"));
                        md.marketStatus = Convert.ToString(data.SelectToken("marketStatus"));
                        matachObj.Add(md);
                    }
                    var XMLData = XmlConversion.SerializeToXElement(matachObj);
                    connection.MatchUpdate(ExeCaption_.ToLower(), XMLData.ToString());

                }
                else if (ExeCaption_.ToLower() == "jobupdateoddsup")
                {
                    List<MatchModel> matchIdList = connection.getMatchId("getMarketMatchupcomming");
                    WebClient wclient = new WebClient();
                    string apiurl = ConfigurationManager.AppSettings["MatchOddApi"];
                    List<matchdata> matachObj = new List<matchdata>();
                    for (int i = 0; i < matchIdList.Count; i++)
                    {
                        string apiurlId = apiurl + matchIdList[i].match_market_id;
                        string apidata = wclient.DownloadString(apiurlId);
                        JArray jsonArray = JArray.Parse(apidata);
                        dynamic data = JObject.Parse(jsonArray[0].ToString());
                        matchdata md = new matchdata();
                        md.marketId = matchIdList[i].match_market_id;
                        md.match_id = matchIdList[i].match_id;
                        md.selection_idHome = "1";
                        md.lastPriceTradedHome = Convert.ToString(data.SelectToken("selections")[0].SelectToken("odds"));
                        md.selection_idAway = "2";
                        md.lastPriceTradedAway = Convert.ToString(data.SelectToken("selections")[1].SelectToken("odds"));
                        md.marketStatus = Convert.ToString(data.SelectToken("marketStatus"));
                        matachObj.Add(md);
                    }
                    var XMLData = XmlConversion.SerializeToXElement(matachObj);
                    connection.MatchUpdate(ExeCaption_.ToLower(), XMLData.ToString());

                }
                else if (ExeCaption_.ToLower() == "matchlive")
                {
                    connection.Matchlive(ExeCaption_.ToLower());
                }
                else if (ExeCaption_.ToLower() == "matchschedule")
                {
                    Tbl_Access_Token tokenData = new Tbl_Access_Token();
                    tokenData = connection.getAccesTokenData();

                    string ballapiurl = ConfigurationManager.AppSettings["scheduleApi"];
                    var schedulApiData = getWebApiCall(ballapiurl + tokenData.access_token);
                    var schdApiDataJson = JObject.Parse(schedulApiData);
                    if (Convert.ToString(schdApiDataJson.SelectToken("status_code")) == "403")
                    {
                        string latestToken = tokencheck(tokenData);
                        string resData = connection.updateToken("updateAccesstoken", latestToken);
                        schedulApiData = getWebApiCall(ballapiurl + tokenData.access_token);
                        schdApiDataJson = JObject.Parse(schedulApiData);
                    }

                    //int datearraywise = int.Parse(DateTime.UtcNow.AddDays(-1).ToString("dd"));
                    int datearraywise = int.Parse(DateTime.UtcNow.ToString("dd"));
                    datearraywise = datearraywise - 1;

                    var matchData = Convert.ToString(schdApiDataJson.SelectToken("data").SelectToken("months")[0].SelectToken("days")[datearraywise]);
                    dynamic jObjMatch = JsonConvert.DeserializeObject(matchData);
                    List<scheduleMatch> matachObj = new List<scheduleMatch>();
                    for (int i = 0; i < jObjMatch.SelectToken("matches").Count; i++)
                    {
                        var matchjson = JObject.Parse(matchData);
                        scheduleMatch schdata = new scheduleMatch();
                        if (matchjson.SelectToken("matches")[i].SelectToken("key").ToString().Contains("ipl"))
                        {
                            schdata.feedkey = Convert.ToString(matchjson.SelectToken("matches")[i].SelectToken("key"));
                            var shortname = Convert.ToString(matchjson.SelectToken("matches")[i].SelectToken("short_name")).Split(' ');
                            schdata.hometeam = Convert.ToString(shortname[0]);
                            schdata.awayeam = Convert.ToString(shortname[2]);
                            schdata.schDate = Convert.ToDateTime(matchjson.SelectToken("matches")[i].SelectToken("start_date").SelectToken("iso"));
                            matachObj.Add(schdata);
                        }
                    }
                    var XMLData = XmlConversion.SerializeToXElement(matachObj);
                    connection.MatchUpdate(ExeCaption_.ToLower(), XMLData.ToString());
                }

                else if (ExeCaption_.ToLower() == "feedscore")
                {
                    //for (int i = 0; i < 80; i++)
                    //{

                    Tbl_Access_Token tokenData = new Tbl_Access_Token();
                    List<MatchModelFeed> matchIdList = connection.getMatchKey("getMatcKey");
                    tokenData = connection.getAccesTokenData();
                    foreach (MatchModelFeed model in matchIdList)
                    {

                        string ballapiurl = ConfigurationManager.AppSettings["ballApi"];
                        //for summarydata
                        var summaryApiData = getWebApiCall(ballapiurl + model.match_score_feed_key + "/?access_token=" + tokenData.access_token + "&card_type=summary_card");
                        var summaryApiDataJson = JObject.Parse(summaryApiData);
                        if (Convert.ToString(summaryApiDataJson.SelectToken("status_code")) == "403")
                        {
                            string latestToken = tokencheck(tokenData);
                            string resData = connection.updateToken("updateAccesstoken", latestToken);
                            summaryApiData = getWebApiCall(ballapiurl + model.match_score_feed_key + "/?access_token=" + latestToken);
                            summaryApiDataJson = JObject.Parse(summaryApiData);
                        }

                        string first_batting = Convert.ToString(summaryApiDataJson.SelectToken("data").SelectToken("card").SelectToken("first_batting"));
                        var battingdata = JObject.Parse(Convert.ToString(summaryApiDataJson.SelectToken("data").SelectToken("card")));

                        if (first_batting == "")
                        {
                            this.Close();
                            return;
                        }
                        scoreFeed feeddata = connection.getFeedData("getMatchFeed", model.match_id);
                        string innings = "";
                        if (feeddata == null)
                        {
                            ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + first_batting + "_1_1" + "/?access_token=";
                            innings = "0";
                        }
                        else
                        {
                            if (feeddata.winner_team != "")
                            //if (feeddata.winner_team == "")
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                //condition when last ball overend and stroe in 
                                if (feeddata.highlight_names_keys.Contains("over_end"))
                                {
                                    if (first_batting == feeddata.batting_team)
                                    {
                                        innings = "0";
                                    }
                                    else
                                    {
                                        innings = "1";
                                    }

                                    if (feeddata.next_over == "")
                                    {
                                        //ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + first_batting + "_1_" + feeddata.overs + "" + "/?access_token=";
                                        //int over = int.Parse(feeddata.overs) + 1;
                                        //
                                        if (first_batting == feeddata.batting_team)
                                        {
                                            ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + first_batting + "_1_" + feeddata.overs + "" + "/?access_token=";
                                        }
                                        else
                                        {
                                            ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + feeddata.batting_team + "_1_" + feeddata.overs + "" + "/?access_token=";
                                        }
                                    }
                                    else
                                    {
                                        ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + feeddata.next_over + "/?access_token=";
                                    }
                                }
                                else
                                {
                                    if (first_batting == feeddata.batting_team)
                                    {
                                        ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + first_batting + "_1_" + feeddata.overs + "" + "/?access_token=";
                                        innings = "0";
                                    }
                                    else
                                    {
                                        ballapiurl = "https://rest.cricketapi.com/rest/v2/match/" + model.match_score_feed_key + "/balls/" + "" + feeddata.batting_team + "_1_" + feeddata.overs + "" + "/?access_token=";
                                        innings = "1";
                                    }
                                }
                            }
                        }

                        var ballapidata = getWebApiCall(ballapiurl + tokenData.access_token);
                        var ballApiDAtaJson = JObject.Parse(ballapidata);

                        //for match not available
                        if (Convert.ToString(ballApiDAtaJson.SelectToken("status_code")) == "404")
                        {
                            this.Close();
                            return;
                        }

                        //for token invalid
                        else if (Convert.ToString(ballApiDAtaJson.SelectToken("status_code")) == "403")
                        {
                            string latestToken = tokencheck(tokenData);
                            string resData = connection.updateToken("updateAccesstoken", latestToken);
                            ballapidata = getWebApiCall(ballapiurl + tokenData.access_token);
                            ballApiDAtaJson = JObject.Parse(ballapidata);

                        }

                        List<scoreFeed> matachObj = new List<scoreFeed>();
                        JObject parent = JObject.Parse(Convert.ToString(ballApiDAtaJson.SelectToken("data")));
                        var companies = parent.Value<JObject>("balls").Properties();
                        foreach (var company in companies)
                        {
                            scoreFeed sfeed = new scoreFeed();
                            sfeed.balls_id = company.Name;
                            sfeed.next_over = Convert.ToString(ballApiDAtaJson.SelectToken("data").SelectToken("next_over"));

                            //get ball unique key data
                            JObject balldata = JObject.Parse(company.Value.ToString());
                            sfeed.comment = Convert.ToString(balldata.SelectToken("comment"));
                            sfeed.wicket_type = Convert.ToString(balldata.SelectToken("wicket_type"));
                            sfeed.ball_type = Convert.ToString(balldata.SelectToken("ball_type"));
                            if (Convert.ToString(balldata.SelectToken("innings")) == "superover")
                            {
                                sfeed.innings = "2";
                            }
                            sfeed.innings = innings;
                            sfeed.ball = Convert.ToString(balldata.SelectToken("ball"));
                            sfeed.overs = Convert.ToString(balldata.SelectToken("over"));
                            sfeed.runs = Convert.ToString(balldata.SelectToken("team").SelectToken("runs"));
                            sfeed.wicket = Convert.ToString(balldata.SelectToken("team").SelectToken("wicket"));
                            sfeed.highlight_names_keys = Convert.ToString(balldata.SelectToken("highlight_names_keys"));
                            sfeed.batting_team = Convert.ToString(balldata.SelectToken("batting_team"));

                            sfeed.winner_team = Convert.ToString(battingdata.SelectToken("winner_team"));
                            sfeed.first_batting = first_batting;
                            sfeed.won_toss = Convert.ToString(battingdata.SelectToken("toss").SelectToken("won"));


                            sfeed.match_result = Convert.ToString(battingdata.SelectToken("msgs").SelectToken("result"));

                            sfeed.jsondata = Convert.ToString(balldata);
                            sfeed.home_team_id = model.home_team_id;
                            sfeed.away_team_id = model.away_team_id;
                            sfeed.match_id = model.match_id;
                            sfeed.match_market_id = model.match_market_id;

                            matachObj.Add(sfeed);
                        }
                        var XMLData = XmlConversion.SerializeToXElement(matachObj.OrderBy(x => x.ball).ToList());
                        connection.MatchUpdate(ExeCaption_.ToLower(), XMLData.ToString());
                    }
                    //i++;
                    //}
                }
            }
            catch (Exception ex)
            { }
            this.Close();
        }


        public string tokencheck(Tbl_Access_Token tokenData)
        {
            string AuthTokenApi = ConfigurationManager.AppSettings["AuthTokenApi"];
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["access_key"] = tokenData.access_key;
                values["secret_key"] = tokenData.secret_key;
                values["app_id"] = tokenData.app_id;
                values["device_id"] = tokenData.device_id;
                var response = client.UploadValues(AuthTokenApi, values);
                var responseString = Encoding.Default.GetString(response);

                var tokenApiData = JObject.Parse(responseString);
                string latestToken = Convert.ToString(tokenApiData.SelectToken("auth").SelectToken("access_token"));
                return latestToken;
            }
        }


        public string getWebApiCall(string url)
        {
            string response = "";
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                var responseTask = client.GetAsync(url);
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    response = readTask.Result.ToString();
                }
            }
            return response;
        }

        public class XmlConversion
        {
            public static XElement SerializeToXElement(object o)
            {
                var doc = new XDocument();
                using (XmlWriter writer = doc.CreateWriter())
                {

                    XmlSerializer serializer = new XmlSerializer(o.GetType());
                    serializer.Serialize(writer, o);
                }
                return doc.Root;
            }
        }
    }
}
