﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match_Update_Cron.ClassData
{
    public class MatchModel
    {
        public string match_market_id { get; set; }
        public string match_id { get; set; }
        public string home_team_id { get; set; }
        public string away_team_id { get; set; }
    }

    public class MatchModelFeed
    {
        public string match_market_id { get; set; }
        public string match_id { get; set; }
        public string home_team_id { get; set; }
        public string away_team_id { get; set; }
        public string match_score_feed_key { get; set; }
    }



    public class matchdata
    {
        public string match_id { get; set; }
        public string marketId { get; set; }
        public string name { get; set; }
        public string countryCode { get; set; }
        public string venue { get; set; }
        public string openDate { get; set; }
        public string runnerNameHome { get; set; }
        public string lastPriceTradedHome { get; set; }

        public DateTime? match_date { get; set; }
        public DateTime? match_time { get; set; }

        public string runnerNameAway { get; set; }
        public string lastPriceTradedAway { get; set; }

        public string selection_idHome { get; set; }
        public string selection_idAway { get; set; }

        public string home_team_id { get; set; }
        public string away_team_id { get; set; }

        public string selectionIdMatchHome { get; set; }
        public string selectionIdMAtchAway { get; set; }

        public string marketStatus { get; set; }

    }

    public class Tbl_Access_Token
    {
        public int Id { get; set; }

        public string access_token { get; set; }

        public string access_key { get; set; }

        public string secret_key { get; set; }

        public string app_id { get; set; }

        public string device_id { get; set; }

        public DateTime? Updated_at { get; set; }

    }

    public class scheduleMatch
    {
        public string hometeam { get; set; }
        public string awayeam { get; set; }
        public string feedkey { get; set; }
        public DateTime? schDate { get; set; }
    }


    public class scoreFeed
    {
        public string feed_score_id { get; set; }
        public string match_id { get; set; }
        public string match_market_id { get; set; }
        public string balls_id { get; set; }
        public string next_over { get; set; }
        public string comment { get; set; }
        public string wicket_type { get; set; }
        public string ball_type { get; set; }
        public string runs { get; set; }

        public string innings { get; set; }
        public string ball { get; set; }
        public string overs { get; set; }
        public string jsondata { get; set; }
        public string home_team_id { get; set; }
        public string away_team_id { get; set; }
        public string highlight_names_keys { get; set; }
        public string wicket { get; set; }
        public string batting_team { get; set; }
        public string winner_team { get; set; }
        public string first_batting { get; set; }
        public string won_toss { get; set; }
        public string match_result { get; set; }
    }
}
