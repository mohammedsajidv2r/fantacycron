﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Match_Update_Cron.ClassData
{
    public class BaseConnection
    {
        //static string connectionString = @"Data Source=cricket24-bet-uat.coyzw2u3p296.ap-south-1.rds.amazonaws.com;Initial Catalog=realtime-fantasy-db;User ID=Cricket24bet;Password=Cricketbet!!24";
        static string connectionString = @"Data Source=sports-cards.coyzw2u3p296.ap-south-1.rds.amazonaws.com;Initial Catalog=realtime-fantasy-db;User ID=sportscardsdb;Password=3WHdmTkvV9m36qzD";
        public void MatchUpdate(string Type, string xml)
        {
            string RES = "";
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@RequestType", Type);
                parameters1.Add("@XmlInput", xml);
                RES = SqlMapper.Query<string>(con, "Jobs_USP_MatchUpdate", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                RES = "";
            }
        }

        public List<MatchModel> getMatchId(string Type)
        {
            IDbConnection con = new SqlConnection(connectionString);
            List<MatchModel> matchid = new List<MatchModel>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", Type);
                matchid = SqlMapper.Query<MatchModel>(con, "Jobs_USP_MatchUpdate", param: parameters, commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
            }
            return matchid;
        }

        public void Matchlive(string Type)
        {
            string RES = "";
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@RequestType", Type);
                RES = SqlMapper.Query<string>(con, "USP_Job_MatchLive", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                RES = "";
            }
        }

        public Tbl_Access_Token getAccesTokenData()
        {
            Tbl_Access_Token token = new Tbl_Access_Token();
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@RequestType", "getAccesstoken");
                token = SqlMapper.Query<Tbl_Access_Token>(con, "Jobs_USP_MatchUpdate", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return token;
        }

        public string updateToken(string Type, string token)
        {
            string RES = "";
            IDbConnection con = new SqlConnection(connectionString);
            try
            {
                DynamicParameters parameters1 = new DynamicParameters();
                parameters1.Add("@RequestType", Type);
                parameters1.Add("@token", token);
                RES = SqlMapper.Query<string>(con, "Jobs_USP_MatchUpdate", param: parameters1, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception ex)
            {
                RES = "";
            }
            return RES;
        }

        public List<MatchModelFeed> getMatchKey(string Type)
        {
            IDbConnection con = new SqlConnection(connectionString);
            List<MatchModelFeed> matchid = new List<MatchModelFeed>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", Type);
                matchid = SqlMapper.Query<MatchModelFeed>(con, "Jobs_USP_MatchUpdate", param: parameters, commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
            }
            return matchid;
        }

        public scoreFeed getFeedData(string Type,string id)
        {
            IDbConnection con = new SqlConnection(connectionString);
            scoreFeed matchid = new scoreFeed();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", Type);
                parameters.Add("@param", id);
                matchid = SqlMapper.Query<scoreFeed>(con, "Jobs_USP_MatchUpdate", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            catch (Exception ex)
            {
            }
            return matchid;
        }


        public static XElement SerializeToXElement(object o)
        {
            var doc = new XDocument();
            using (XmlWriter writer = doc.CreateWriter())
            {

                XmlSerializer serializer = new XmlSerializer(o.GetType());
                serializer.Serialize(writer, o);
            }
            return doc.Root;
        }
    }
}
